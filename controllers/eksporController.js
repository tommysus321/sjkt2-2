const fs = require('fs');
const mongoose = require("mongoose");
const excel = require('exceljs');
//const url = "mongodb://localhost:27017/"

// Create a connection to the MongoDB database
mongoose.connect("mongodb://localhost:27017/", { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
  if (err) throw err;

// export excel
let db = client.db('db_sjkt');
db.collection('pegawai').find({}).toArray(function(err, result){
  if(err) throw err;
  console.log(result);
  client.close();

    let workbook = new excel.Workbook(); //creating workbook
	let worksheet = workbook.addWorksheet('pegawai'); //creating worksheet
	
//  WorkSheet Header
worksheet.columns = [
	{ header: 'Id', key: '_id', width: 10 },
	{ header: 'Nama', key: 'nama', width: 30 },
	{ header: 'nomor_wa', key: 'nomor_wa', width: 30},
	{ header: 'tanggal_pkt', key: 'tanggal_piket', width: 10, outlineLevel: 1}
  ];

// Add Array Rows
worksheet.addRows(result);

// Write to File
workbook.xlsx.writeFile("jadwalpiket.xlsx")
	.then(function() {
		console.log("file saved!");
		});
	
    db.close();
});
});
